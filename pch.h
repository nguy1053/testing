/**
 * @file pch.h
 * @author Nam Hai Nguyen
 *
 *
 */

#ifndef ACTIONSUDOKU__PCH_H
#define ACTIONSUDOKU__PCH_H

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#endif //ACTIONSUDOKU__PCH_H
