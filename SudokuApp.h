/**
 * @file SudokuApp.h
 * @author Nam Hai Nguyen
 *
 * Main application class
 */

#ifndef ACTIONSUDOKU__SUDOKUAPP_H
#define ACTIONSUDOKU__SUDOKUAPP_H

/**
 * Main Application Class
 */
class SudokuApp :public wxApp
{
public:
    virtual bool OnInit();
};

#endif //ACTIONSUDOKU__SUDOKUAPP_H
