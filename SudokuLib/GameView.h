/**
 * @file GameView.h
 * @author Nam Hai Nguyen
 *
 * Class that implements the child window our program draws in.
 */

#ifndef ACTIONSUDOKU_SUDOKULIB_GAMEVIEW_H
#define ACTIONSUDOKU_SUDOKULIB_GAMEVIEW_H

#include "Game.h"

class GameView : public wxWindow
{
private:

public:
    void Initialize(wxFrame *mainFrame);
};

#endif //ACTIONSUDOKU_SUDOKULIB_GAMEVIEW_H
