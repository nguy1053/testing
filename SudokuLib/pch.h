/**
 * @file pch.h
 * @author Nam Hai Nguyen
 *
 *
 */

#ifndef ACTIONSUDOKU_SUDOKULIB_PCH_H
#define ACTIONSUDOKU_SUDOKULIB_PCH_H

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#endif //ACTIONSUDOKU_SUDOKULIB_PCH_H
